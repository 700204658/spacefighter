
#include "BioEnemyShip.h"


BioEnemyShip::BioEnemyShip()
{
	SetSpeed(150);
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}

//updates the player ship so that they can move
void BioEnemyShip::Update(const GameTime *pGameTime)
{
	//checks if the ship is active
	if (IsActive())
	{
		//makes the ship fly in a wave pattern
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		//sets how fast the ship is moving
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		//places the ship at the correct position on the map
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());
		//deactivates it once it is off the screen
		if (!IsOnScreen()) Deactivate();
	}
	//calls it again so it will keep updating
	EnemyShip::Update(pGameTime);
}


void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
